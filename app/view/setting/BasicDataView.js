Ext.define('Pertemuan.view.setting.BasicDataView', {
    extend: 'Ext.Container',
    xtype: 'basicdataview',
    requires: [        
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Pertemuan.store.Personnel',
        'Ext.field.Search',
        'Ext.SegmentedButton',
        'Ext.Toolbar',
        'Ext.Spacer'
    ],

    viewModel: {
        stores: {
            personnel: {
                type: 'personnel'
            }
        }
    },

    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    items: [{
            docked: 'top',
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'searchfield',
                    placeHolder: 'Search',
                    name: 'searchfield',
                    listeners: {
                        change: function ( me, newValue, oldValue, eOpts ){                            
                            personnelStore = Ext.getStore('personnel');
                            personnelStore.filter('name', newValue);
                        }
                    }
                },
                {
                    xtype: 'spacer'
                },
                {
                    text: 'Tambah Resep',
                    ui: 'action',
                    handler: function(){
                        this.overlay = Ext.Viewport.add({
                            xtype: 'login',
                            floated: true,
                            modal: true,
                            hideOnMaskTap: true,
                            showAnimation: {
                                type: 'popIn',
                                duration: 250,
                                easing: 'ease-out'
                            },
                            hideAnimation: {
                                type: 'popOut',
                                duration: 250,
                                easing: 'ease-out'
                            },
                            centered: true,
                            width: Ext.filterPlatform('ie10') ? '100%' : (Ext.os.deviceType == 'Phone') ? 260 : 400,                            
                            maxHeight: Ext.filterPlatform('ie10') ? '30%' : (Ext.os.deviceType == 'Phone') ? 220 : 400,
                            //styleHtmlContent: true,
                            //html: '<p>This is a modal, centered and floated panel. hideOnMaskTap is true by default so ' +
                            //'we can tap anywhere outside the overlay to hide it.</p>',
                            //header: {
                            //    title: 'Overlay Title'
                            //},
                            scrollable: true
                        });
                        this.overlay.show();                    
                    }
                }
            ]
        },{
            xtype: 'dataview',
            scrollable: 'y',
            cls: 'dataview-basic',
            itemTpl: '<font size=4 color="red">{npm}</font><br><b>{name}</b><br><i>{email}</i>-<u>{phone}</u><hr>',
            bind: {
                store: '{personnel}',
            },
            plugins: {
                type: 'dataviewtip',
                align: 'l-r?',
                plugins: 'responsive',
                
                // On small form factor, display below.
                responsiveConfig: {
                    "width < 600": {
                        align: 'tl-bl?'
                    }
                },
                width: 600,
                minWidth: 300,
                //delegate: '.img',
                allowOver: true,
                anchor: true,
                bind: '{record}',
                tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
                        '<tr><td>NPM: </td><td>{npm}</td></tr>' +
                        '<tr><td>Name:</td><td>{name}</td></tr>' + 
                        '<tr><td>Email:</td><td>{email}</td></tr>' + 
                        '<tr><td>Phone:</td><td>{phone}</td></tr>'
            }
    }]
});