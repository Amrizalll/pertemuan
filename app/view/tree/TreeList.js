Ext.define('Pertemuan.view.tree.TreeList', {
    extend: 'Ext.grid.Tree',
    xtype: 'tree-list',
    requires: [
        'Ext.grid.plugin.MultiSelection'
    ],

    
    cls: 'demo-solid-background',
    shadow: true,

    viewModel: {
        type: 'tree-list'
    },

    bind: '{navItems}',

    listeners: {
        itemtap: function ( me, index, target, record, e, eOpts ){
            detailtree = Ext.getCmp('detailtree');
            detailtree.setHtml("Anda memilih "+record.data.text);
        }
    }
});