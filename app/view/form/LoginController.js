Ext.define('Pertemuan.view.form.LoginController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.login',

    onLogin: function() {
        var form = this.getView();
        var username = form.getFields('username').getValue();
        var password = form.getFields('password').getValue();
        //Cek username password sesuai dengan database
        localStorage.setItem('logeddin', true);
        form.hide(); 
    }
})