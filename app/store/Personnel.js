Ext.define('Pertemuan.store.Personnel', {
    extend: 'Ext.data.Store',
    storeId: 'personnel',
    alias: 'store.personnel',

    fields: [
        'npm', 'name', 'email', 'phone'
    ],

    autoLoad: true,
    proxy: {
        type: 'jsonp',
        api: {
            read    : Pertemuan.util.Globals.getPhppath()+'/php/readPersonnel.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'items',
            messageProperty: 'error'
        }
    }
});
